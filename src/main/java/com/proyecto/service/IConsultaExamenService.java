package com.proyecto.service;

import java.util.List;

import com.proyecto.model.ConsultaExamen;

public interface IConsultaExamenService {
	
	List<ConsultaExamen> listarExamenesPorConsulta(Integer idconsulta);

}
