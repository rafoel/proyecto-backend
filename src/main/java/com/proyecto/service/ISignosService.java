package com.proyecto.service;

import com.proyecto.model.Signos;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface ISignosService extends ICRUD<Signos> {

    Page<Signos> listarPageable(Pageable pageable);
}
