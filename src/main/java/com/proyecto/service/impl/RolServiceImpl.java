package com.proyecto.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.proyecto.dao.IRolDAO;
import com.proyecto.model.Rol;
import com.proyecto.service.IRolService;

@Service
public class RolServiceImpl implements IRolService {

    @Autowired
    private IRolDAO dao;

    @Override
    public Page<Rol> listarPageable(Pageable pageable) {
        return dao.findAll(pageable);
    }

    @Override
    public Rol getUltimo() {
    	List<Rol> listaRol = listar();
		return listaRol.get(listaRol.size() - 1);
    }

    @Override
    public Rol registrar(Rol rol) {
        return dao.save(rol);
    }

    @Override
    public Rol modificar(Rol rol) {
        return dao.save(rol);
    }

    @Override
    public void eliminar(int idRol) {
    	dao.deleteById(idRol);
    }

    @Override
    public Rol listarId(int idRol) {
    	Optional<Rol> opt = dao.findById(idRol);
		return opt.isPresent() ? opt.get() : new Rol();
    }

    @Override
    public List<Rol> listar() {
        return dao.findAll();
    }
}
