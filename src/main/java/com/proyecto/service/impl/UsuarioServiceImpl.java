package com.proyecto.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.proyecto.dao.IUsuarioDAO;
import com.proyecto.model.Usuario;
import com.proyecto.service.IUsuarioService;

@Service
public class UsuarioServiceImpl implements IUsuarioService {
    @Autowired
    private IUsuarioDAO dao;

    @Override
    public Page<Usuario> listarPageable(Pageable pageable) {
        return dao.findAll(pageable);
    }

    @Override
    public Usuario registrar(Usuario usuario) {
        return dao.save(usuario);
    }

    @Override
    public Usuario modificar(Usuario usuario) {
        return dao.save(usuario);
    }

    @Override
    public void eliminar(int idUsuario) {
        dao.deleteById(idUsuario);
    }

    @Override
    public Usuario listarId(int idUsuario) {
    	Optional<Usuario> opt = dao.findById(idUsuario);
		return opt.isPresent() ? opt.get() : new Usuario();
    }

    @Override
    public List<Usuario> listar() {
        return dao.findAll();
    }
}
