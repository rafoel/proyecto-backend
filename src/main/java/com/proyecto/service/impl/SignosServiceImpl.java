package com.proyecto.service.impl;

import com.proyecto.dao.ISignosDAO;
import com.proyecto.model.Menu;
import com.proyecto.model.Signos;
import com.proyecto.service.ISignosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class SignosServiceImpl implements ISignosService {

    @Autowired
    private ISignosDAO dao;

    @Override
    public Page<Signos> listarPageable(Pageable pageable) {
        return dao.findAll(pageable);
    }

    @Override
    public Signos registrar(Signos signos) {
        return dao.save(signos);
    }

    @Override
    public Signos modificar(Signos signos) {
        return dao.save(signos);
    }

    @Override
    public void eliminar(int idSigno) {
    	dao.deleteById(idSigno);
    }

    @Override
    public Signos listarId(int idSigno) {
    	Optional<Signos> opt = dao.findById(idSigno);
		return opt.isPresent() ? opt.get() : new Signos();
    }

    @Override
    public List<Signos> listar() {
        return dao.findAll();
    }
}
