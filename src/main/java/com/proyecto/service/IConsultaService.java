package com.proyecto.service;

import java.util.List;

import com.proyecto.dto.ConsultaListaExamenDTO;
import com.proyecto.dto.ConsultaResumenDTO;
import com.proyecto.dto.FiltroConsultaDTO;
import com.proyecto.model.Consulta;

public interface IConsultaService {

	Consulta registrar(ConsultaListaExamenDTO consultaDTO);

	void modificar(Consulta consulta);

	void eliminar(int idConsulta);

	Consulta listarId(int idConsulta);

	List<Consulta> listar();
	
	List<Consulta> buscar(FiltroConsultaDTO filtro);

	List<Consulta> buscarfecha(FiltroConsultaDTO filtro);
	
	List<ConsultaResumenDTO> listarResumen();

	byte[] generarReporte();
}
