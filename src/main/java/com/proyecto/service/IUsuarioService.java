package com.proyecto.service;

import com.proyecto.model.Usuario;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface IUsuarioService  extends ICRUD<Usuario>  {
    Page<Usuario> listarPageable(Pageable pageable);

}
