package com.proyecto.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.proyecto.model.Archivo;

@Repository
public interface IArchivoDAO extends JpaRepository<Archivo, Integer>{

}