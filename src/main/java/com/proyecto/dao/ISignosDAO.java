package com.proyecto.dao;

import com.proyecto.model.Signos;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ISignosDAO extends JpaRepository<Signos, Integer> {

}
