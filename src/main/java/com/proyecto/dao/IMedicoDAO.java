package com.proyecto.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.proyecto.model.Medico;

@Repository
public interface IMedicoDAO extends JpaRepository<Medico, Integer> {

}
