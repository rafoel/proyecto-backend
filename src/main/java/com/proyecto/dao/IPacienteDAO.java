package com.proyecto.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.proyecto.model.Paciente;

@Repository
public interface IPacienteDAO extends JpaRepository<Paciente, Integer>{

}
