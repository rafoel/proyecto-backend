package com.proyecto.dto;

import java.util.List;

import com.proyecto.model.Paciente;

public class Paciente2DTO {

	private List<Paciente> pacientes;

	public List<Paciente> getPacientes() {
		return pacientes;
	}

	public void setPacientes(List<Paciente> pacientes) {
		this.pacientes = pacientes;
	}

}
