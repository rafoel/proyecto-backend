package com.proyecto.controller;

import com.proyecto.exception.ModeloNotFoundException;
import com.proyecto.model.Rol;
import com.proyecto.service.IRolService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@RestController
@RequestMapping("/roles")
public class RolController {

    @Autowired
    private IRolService service;

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Rol>> listar() {
        List<Rol> roles = new ArrayList<>();

        roles = service.listar();

        return new ResponseEntity<List<Rol>>(roles, HttpStatus.OK);
    }

    @GetMapping(value = "/pageable", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Page<Rol>> listarPageable(Pageable pageable) {
        Page<Rol> roles = null;

        roles = service.listarPageable(pageable);

        return new ResponseEntity<Page<Rol>>(roles, HttpStatus.OK);
    }

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Resource<Rol> listarId(@PathVariable("id") Integer id) {
        Rol rol = new Rol();
        rol = service.listarId(id);
        if (rol == null) {
            throw new ModeloNotFoundException("ID: " + id);
        }

        Resource<Rol> resource = new Resource<Rol>(rol);
        ControllerLinkBuilder linkTo = linkTo(methodOn(this.getClass()).listar());
        resource.add(linkTo.withRel("all-roles"));
        return resource;
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> registrar(@Valid @RequestBody Rol rol) {
        Rol ultimo = service.getUltimo();
        rol.setIdRol(ultimo.getIdRol() + 1);
        Rol xRol = service.registrar(rol);
        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
                .buildAndExpand(xRol.getIdRol()).toUri();

        return ResponseEntity.created(location).build();
    }

    @PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> actualizar(@RequestBody Rol rol) {
        service.modificar(rol);
        return new ResponseEntity<Object>(HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public void eliminar(@PathVariable Integer id) {
        Rol pac = service.listarId(id);
        if (pac == null) {
            throw new ModeloNotFoundException("ID: " + id);
        } else {
            service.eliminar(id);
        }
    }
}
